import base64
from prob0210 import CBC
from prob0209 import PKCS7
from Cryptodome import Random
from Cryptodome.Random import random
from Cryptodome.Cipher import AES
from Cryptodome.Cipher.AES import block_size


key = Random.new().read(block_size)
thispkcs7 = PKCS7()
cipher = CBC()
IV = b'\x00'*AES.block_size

def quote_quote(string):
    
    string ="comment1=cooking%20MCs;userdata=" + string + ";comment2=%20like%20a%20pound%20of%20bacon" 
    
    result = ""
    for i in string:
        if i == '=':
            i ='"="'
        elif i == ';':
            i = '";"'
        else:
            i = i
        result += i
    
    padded_result = thispkcs7.PKCS7_padding(result, block_size)
    return cipher.encrypt(padded_result, key, IV)

def unquote_unquote(ciphertext):
    
    ciphertext = bytearray(ciphertext)
    ciphertext[27] = ciphertext[27]^0x02
    
    bit_flip_plaintext = cipher.decrypt(ciphertext, key, IV)
    if b"admin=true" in bit_flip_plaintext:
        answer = "yes"
    else:
        answer = "no"
    
    return (bit_flip_plaintext, answer)


if __name__=="__main__":
    ciphertext = quote_quote("admin?true")
    answer = unquote_unquote(ciphertext)
    print("Is 'admin=true' in new plaintext? ", answer[1])

